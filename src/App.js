import './App.css';
import { Switch, Route } from 'react-router-dom';
import { useState } from 'react';

import Home from './pages/home';
import PersonDetails from './pages/personDetails';

const App = () => {

  const [username, setUsername] = useState("");
  const [nome, setNome] = useState("");
  const [email, setEmail] = useState("");
  const [senha, setSenha] = useState("");
  const [cpf, setCpf] = useState("");
  const [telefone, setTelefone] = useState("");
  const [cep, setCep] = useState("");

  return (
    <div className="App">
      <header className="App-header">
       <Switch>
         <Route exact path="/">
            <Home username={username} setUsername={setUsername} nome={nome} setNome={setNome} email={email} setEmail={setEmail}
              senha={senha} setSenha={setSenha} cpf={cpf} setCpf={setCpf} telefone={telefone} setTelefone={setTelefone} 
              cep ={cep} setCep={setCep}/>            
         </Route>
         <Route path="/details">
            <PersonDetails username={username} nome={nome} email={email} cpf={cpf} telefone={telefone} cep={cep}/>
         </Route>
       </Switch>
      </header>
    </div>
  );
}

export default App;
