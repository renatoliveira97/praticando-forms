import React from 'react';

class PersonDetails extends React.Component {
    render () {

        const {username, nome, email, cpf, telefone, cep} = this.props;

        return (
            <div className="card">
                <h1 className="username">{username}</h1>
                <p>Nome: {nome}</p>
                <p>Email: {email}</p>
                <p>Cpf: {cpf}</p>
                <p>Telefone: {telefone}</p>
                <p>Cep: {cep}</p>
            </div>
        );
    }
}

export default PersonDetails;