import * as yup from 'yup';
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';

import { useHistory } from 'react-router';

const Home = ({username, setUsername, nome, setNome, email, setEmail, senha, setSenha, cpf, setCpf, telefone, 
    setTelefone, cep, setCep}) => {

    const formSchema = yup.object().shape({
        username: yup.string().required("Nome de usuário obrigatório"),
        nome: yup.string().required("Nome obrigatório"),
        email: yup.string().required("E-mail obrigatório").email("E-mail inválido"),
        confirmEmail: yup.string().oneOf([yup.ref("email")], "Os emails devem ser iguais").required("Confirmação de email obrigatório"),
        senha: yup.string().required("Senha obrigatória"),
        confirmSenha: yup.string().oneOf([yup.ref('senha')], "As senhas devem ser iguais").required("Confirmação de senha obrigatória"),
        cpf: yup.string().matches(/^[0-9]{11}$/, "Digite um cpf com 11 digitos").required("Cpf obrigatório"),
        telefone: yup.string().matches(/^[0-9]{10,11}$/, "Digite um número de telefone com 10 a 11 digitos").required("Telefone obrigatório"),
        cep: yup.string().matches(/^[0-9]{8}$/, "Digite um cep com 8 números").required("Cep obrigatório")
    });
        
    const {
        register,
        handleSubmit,
        formState: { errors }
    } = useForm({
        resolver: yupResolver(formSchema)
    });
        
    const history = useHistory();
    const onSubmitFunction = (data) => history.push({pathname: "/details", state: data});
        

    return (
        <>
            <form className="form" onSubmit={handleSubmit(onSubmitFunction)}>
                <input
                    placeholder="Nome de usuário*"
                    {...register("username")}
                    type="text"
                    value={username}
                    onChange={(e) => setUsername(e.target.value)}></input>
                    <p className="yup_error">{errors.username?.message}</p>
                <input
                    placeholder="Nome*"
                    {...register("nome")}
                    type="text"
                    value={nome}
                    onChange={(e) => setNome(e.target.value)}></input>
                    <p className="yup_error">{errors.nome?.message}</p>
                <input
                    placeholder="Email*"
                    {...register("email")}
                    type="text"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}></input>
                    <p className="yup_error">{errors.email?.message}</p>
                <input
                    placeholder="Confirme seu email*"
                    {...register("confirmEmail")}
                    type="text"></input>
                    <p className="yup_error">{errors.confirmEmail?.message}</p>
                <div className="container_senha">
                    <div className="senha">
                        <input
                            placeholder="Senha*"
                            {...register("senha")}
                            type="text"
                            value={senha}
                            onChange={(e) => setSenha(e.target.value)}></input>
                            <p className="yup_error">{errors.senha?.message}</p>
                    </div>
                    <div className="confirmacao_senha">
                        <input
                            placeholder="Confirme sua senha*"
                            {...register("confirmSenha")}
                            type="text"></input>
                            <p className="yup_error">{errors.confirmSenha?.message}</p>
                    </div>
                </div>
                <input
                    placeholder="Cpf*"
                    {...register("cpf")}
                    type="text"
                    value={cpf}
                    onChange={(e) => setCpf(e.target.value)}></input>
                    <p className="yup_error">{errors.cpf?.message}</p>
                <input
                    placeholder="Telefone*"
                    {...register("telefone")}
                    type="text"
                    value={telefone}
                    onChange={(e) => setTelefone(e.target.value)}></input>
                    <p className="yup_error">{errors.telefone?.message}</p>
                <input
                    placeholder="Cep*"
                    {...register("cep")}
                    type="text"
                    value={cep}
                    onChange={(e) => setCep(e.target.value)}></input>
                    <p className="yup_error">{errors.cep?.message}</p>
                
                <button
                    type="submit">Enviar</button>
            </form>
        </>
    )
}

export default Home;